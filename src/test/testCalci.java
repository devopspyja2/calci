package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.calci;

class testCalci {

	@Test
	void testAdd() {
		assertEquals(30, calci.add(10,20));
	}
	@Test
	void testSub() {
		assertEquals(10, calci.sub(20,10));
	}
	@Test
	void testMul() {
		assertEquals(200, calci.sub(20,10));
	}
	@Test
	void testDev() {
		assertEquals(2, calci.devi(20,10));
		assertEquals(0, calci.devi(20,0));
	}

}
